package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ResponseEntityControllerTest {

    @Autowired
    public MockMvc mockMvc;

    @Test
    void should_create_responseEntity() throws Exception {
        mockMvc.perform(post("/api/student")
                .param("x-id", "201"))
                .andExpect(status().isCreated());
    }

    @Test
    void should_return_student_name_and_phoneNumber() throws Exception {
        String studentJson = "{\"name\":\"xz\",\"age\":18,\"phone_number\":123456789}";
        mockMvc.perform(post("/api/student")
                .content(studentJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("{\"name\":\"xz\",\"age\":18,\"phone_number\":123456789}"));
    }
}
