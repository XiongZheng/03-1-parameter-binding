package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Controller.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class JsonTest {

    @Autowired
    public MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    void set() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void should_serialize_number() throws JsonProcessingException {
        Integer num = 1;
        String json = objectMapper.writeValueAsString(num);
        assertEquals(json, "1");
    }

    @Test
    void should_deserialize_json() throws IOException {
        String json = "1";
        Integer num = objectMapper.readValue(json, Integer.class);
        assertEquals(num.intValue(), 1);
    }

    @Test
    void should_serialize_student() throws JsonProcessingException {
        Student student = new Student();
        student.setName("xz");
        student.setGender("female");
        student.setAge(20);
        String json = objectMapper.writeValueAsString(student);
        assertEquals(json, "{\"name\":\"xz\",\"gender\":\"female\",\"age\":20,\"klass\":0}");
    }

    @Test
    void should_deserialize_student_json() throws IOException {
        String studentJson = "{\"name\":\"xz\",\"gender\":\"female\"}";
        Student student = objectMapper.readValue(studentJson, Student.class);
        assertEquals(student.getName(), "xz");
        assertEquals(student.getGender(), "female");
    }

    @Test
    void should_return_student_with_name() throws Exception {
        mockMvc.perform(
                post("/api/students/all")
                        .content("{\"name\":\"xz\",\"gender\":\"female\",\"age\":20}")
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("xz"));
    }

    @Test
    void should_throw_bad_request_when_name_is_null() throws Exception {
        mockMvc.perform(post("/api/students/all")
                .content("{\"gender\":\"female\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_throw_bad_request_when_age_is_not_valid() throws Exception {
        mockMvc.perform(post("/api/students/age")
                .content("{\"name\":\"xz\",\"gender\":\"female\",\"age\":3}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }
}
