package com.twuc.webApp.ControllerTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Controller.Contract;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

    @Autowired
    MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setObjectMapper() {
        objectMapper = new ObjectMapper();
    }

    //2.1
    @Test
    void should_return_path_variable() throws Exception {
        mockMvc.perform(get("/api/users/1"))
                .andExpect(content().string("user 1"));
    }

    //2.1
    @Test
    void should_return_path_variable_private() throws Exception {
        mockMvc.perform(get("/api/users1/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("user 1"));
    }

    //2.1
    @Test
    void should_return_two_path_variables() throws Exception {
        mockMvc.perform(get("/api/users/1/books/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("userId: 1 ; bookId: 1"));
    }

    //2.2
    @Test
    void should_ensure_string_bind_param() throws Exception {
        mockMvc.perform(get("/api/users")
                .param("userId", "1")
                .param("bookId", "1"))
                .andExpect(status().isOk())
                .andExpect(content().string("userId: 1 ; bookId: 1"));
    }

    // test section 2.3
    @Test
    void should_serialize_contract() throws IOException {
        String contractString = "{\"name\":\"xz\",\"phone\":123456789}";
        Contract contract = objectMapper.readValue(contractString, Contract.class);
        assertEquals(contract.getName(), "xz");
        assertEquals(contract.getPhone(), Long.valueOf(123456789L));
    }

    @Test
    void should_deserialize_contract_json() throws JsonProcessingException {
        Contract contract = new Contract();
        contract.setName("xz");
        contract.setPhone(123456789L);
        String contractString = objectMapper.writeValueAsString(contract);
        assertEquals(contractString, "{\"name\":\"xz\",\"phone\":123456789,\"email\":null}");
    }

    @Test
    void should_return_date() throws Exception {
        mockMvc.perform(post("/api/datetime")
                .content("{\"dateTime\":\"2019-10-01T10:00:00Z\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.dateTime").value("2019-10-01T10:00:00Z"));

    }

    //2.4
    @Test
    void should_verify_not_null() throws Exception {
        mockMvc.perform(post("/api/contract")
                .content("{\"name\":\"xz\",\"phone\":123456789,\"birthday\":1996}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("xz"));
    }

    @Test
    void should_verify_null() throws Exception {
        mockMvc.perform(post("/api/contract")
                .content("{\"telPhone\":123456789}")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_size() throws Exception {
        mockMvc.perform(post("/api/contract")
                .content("{\"name\":\"xzs\",\"telPhone\":123456789}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_max() throws Exception {
        mockMvc.perform(post("/api/contract/birthday")
                .content("{\"name\":\"xz\",\"phone\":123456789,\"birthday\":99999}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_min() throws Exception {
        mockMvc.perform(post("/api/contract/birthday")
                .content("{\"name\":\"xz\",\"phone\":123456789,\"birthday\":100}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_invalid_email() throws Exception {
        mockMvc.perform(post("/api/contract/email")
                .content("{\"name\":\"xz\",\"phone\":123456789,\"birthday\":100,\"email\":\"abc\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_verify_valid_email() throws Exception {
        mockMvc.perform(post("/api/contract/email")
                .content("{\"name\":\"xz\",\"phone\":123456789,\"birthday\":1996,\"email\":\"2496529569@qq.com\"}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }
}
