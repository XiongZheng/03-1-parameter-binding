package com.twuc.webApp.Controller;

import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class Controller {

    //2.1
    @GetMapping("/api/users/{userId}")
    public String getUserWithId(@PathVariable Integer userId){
        return "user " + userId;
    }

    //2.1
    @GetMapping("/api/users1/{userId}")
    public String getUserWithPrimitiveId(@PathVariable int userId){
        return "user " + userId;
    }

    //2.1
    @GetMapping("/api/users/{userId}/books/{bookId}")
    public String getUserAndBookWithId(@PathVariable int userId, @PathVariable int bookId){
        return "userId: " + userId + " ; bookId: " + bookId;
    }

    //2.2
    @GetMapping("/api/users")
    public String getUserAndBookRequestParam(@RequestParam int userId,@RequestParam int bookId){
        return "userId: " + userId + " ; bookId: " + bookId;
    }

    //2.3
    @PostMapping("/api/datetime")
    public DateTime getDateTime(@RequestBody DateTime dateTime){
        return dateTime;
    }

    //2.4
    @PostMapping("/api/contract")
    public String getContract(@RequestBody @Valid Contract contract){
        return contract.getName();
    }

    //2.4
    @PostMapping("/api/contract/birthday")
    public int getBirthday(@RequestBody @Valid Contract contract){
        return contract.getBirthday(1996);
    }

    @PostMapping("/api/contract/email")
    public String getEmail(@RequestBody @Valid Contract contract){
        return contract.getEmail();
    }
}
