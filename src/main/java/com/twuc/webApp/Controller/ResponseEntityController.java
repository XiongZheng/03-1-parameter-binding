package com.twuc.webApp.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResponseEntityController {

    @PostMapping("/api/student")
    public ResponseEntity<Object> getResponseEntity(@RequestBody StudentJson studentJson){
        return ResponseEntity.status(201).body(studentJson);
    }
}
