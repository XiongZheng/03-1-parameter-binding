package com.twuc.webApp;

import com.twuc.webApp.Controller.Student;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class Json {
    @PostMapping("/api/students/all")
    public String getStudentWithName(@RequestBody @Valid Student student){
        return student.getName();
    }

    @PostMapping("/api/students/age")
    public int getStudentWithAge(@RequestBody @Valid Student student){
        return student.getAge();
    }
}
